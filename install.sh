loadkeys uk && echo "Loaded UK key layout"
timedatectl set-ntp true && echo "Synced time with NTP"

echo "Partitioning disk"
lsblk | grep -w sda && parted /dev/sda mklabel gpt || echo "Could not set disk label"; exit 1
parted --align optimal /dev/sda mkpart efi 1MiB 513MiB
parted --align optimal /dev/sda mkpart boot ext3 513MiB 1537MiB
parted --align optimal /dev/sda mkpart lvm 1537MiB 100% 
parted /dev/sda set 1 esp on
parted /dev/sda set 3 lvm on

echo "Creating lvm volumes"
pvcreate /dev/sda3
vgcreate system /dev/sda3
lvcreate -L 8G system -n swap
lvcreate -l +100%FREE system -n root

echo "Creating filesystems"
mkfs.ext4 /dev/system/root
mkfs.ext3 /dev/sda2
mkswap /dev/system/swap

echo "Mounting volumes"
mount /dev/system/root /mnt
mkdir /mnt/boot || echo "Could not create boot directory"; exit 1
mount /dev/sda2 /mnt/boot
mkdir /mnt/boot/EFI || echo "Could not create EFI directory"; exit 1
mount /dev/sda1 /mnt/boot/EFI
swapon /dev/system/swap

mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
reflector --latest 10 --sort rate --save /etc/pacman.d/mirrorlist

pacstrap /mnt base linux linux-firmware emacs lvm2 git

genfstab -U /mnt >> /mnt/etc/fstab
